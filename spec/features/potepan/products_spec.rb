require 'rails_helper'

RSpec.feature "Potepans", type: :feature do
  let(:product) do
    create(:product, name: 'T-shirts',
                     price: '10.00',
                     description: 'This is a popular T-shirts!')
  end

  describe '商品詳細ページのテンプレート' do
    before do
      visit potepan_product_path(product.id)
    end

    it "対象商品のデータが表示される" do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end
  end
end
