require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  include ApplicationHelper
  describe "商品詳細ページへのアクセス" do
    let(:product) { create(:product, name: 'T-shirts') }

    before do
      get :show, params: { id: product.id }
    end

    it "200 OKを返す" do
      expect(response).to have_http_status(:ok)
    end

    it "インスタンス変数が期待される値を持つ" do
      expect(assigns(:product)).to eq product
    end

    it "showテンプレートが表示される" do
      expect(response).to render_template :show
    end
  end
end
